'use strict'

// puntuaciones
const puntuaciones = [
    {
        equipo: 'Toros Negros',
        puntos: [1, 3, 4, 2, 10, 8],
    },
    {
        equipo: 'Amanecer Dorado',
        puntos: [8, 5, 2, 4, 7, 5, 3],
    },
    {
        equipo: 'Águilas Plateadas',
        puntos: [5, 8, 3, 2, 5, 3],
    },
    {
        equipo: 'Leones Carmesí',
        puntos: [5, 4, 3, 1, 2, 3, 4],
    },
    {
        equipo: 'Rosas Azules',
        puntos: [2, 1, 3, 1, 4, 3, 4],
    },
    {
        equipo: 'Mantis Verdes',
        puntos: [1, 4, 5, 1, 3],
    },
    {
        equipo: 'Ciervos Celestes',
        puntos: [3, 5, 1, 1],
    },
    {
        equipo: 'Pavos Reales Coral',
        puntos: [2, 3, 2, 1, 4, 3],
    },
    {
        equipo: 'Orcas Moradas',
        puntos: [2, 3, 3, 4],
    },
]

// # Ejercicio JavaScript 2

/* Edita el archivo script.js para crear una función que reciba una array de objetos (equipos y puntos conseguidos) y muestre
por consola el ** nombre ** del que más y del que menos puntos hayn conseguido, con sus respectivos ** totales **.*/

// Encontrarás un array de ejemplo en el propio documento.


const resultsSeason = (arr) => {
    const teams = []
    // Creo un array de objetos donde le pusheo la info del array original y además los puntos totales y la media
    for (let i = 0; i < arr.length; i++) {
        const team = puntuaciones[i].equipo
        const points = puntuaciones[i].puntos
        let totalPoints = points.reduce((previus, current) => current += previus)
        let pointsAvg = totalPoints / points.length
        teams.push({
            team,
            points,
            pointsAvg,
            totalPoints
        })
    }
    // Creo una funcion donde me ordena el array de mayor a menor valor de la key pointsAgv y devuelvo el resultado
    const winner = () => {
        teams.sort((a, b) => b.pointsAvg - a.pointsAvg)
        const bestTeam = teams[0]
        console.log(`🏆  El mejor equipo de esta temporada ha sido: ${bestTeam.team} con un total de ${bestTeam.totalPoints} puntos y una media de ${(bestTeam.pointsAvg.toFixed(1))} puntos por partido.`)
    }
    winner()
    // Creo una funcion donde me ordena el array de menor a mayor valor de la key pointsAgv y devuelvo el resultado
    const loser = () => {
        teams.sort((a, b) => a.pointsAvg - b.pointsAvg)
        const bestTeam = teams[0]
        console.log(`😞 El peor equipo de esta temporada ha sido: ${bestTeam.team} con un total de ${bestTeam.totalPoints} puntos y una media de ${bestTeam.pointsAvg.toFixed(1)} puntos por partido.`)
    }
    loser()
}
resultsSeason(puntuaciones)

