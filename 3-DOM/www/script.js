'use strict'

// # Ejercicio JavaScript 2

// ** Sin modificar el body en el archivo index.html **, utiliza javascript para crear un div en el que se muestre un reloj con la hora actual y que se actualice automáticamente cada segundo.

// El formato del reloj debe ser HH:MM:SS (siempre dos dígitos por cada unidad).

// Si quieres, puedes añadir estilos para que se vea más bonito, pero no se valorará.




const actualizarHora = function() {
    let fecha = new Date()
    let hora = fecha.getHours()
    if (hora < 10) {
        hora = '0' + hora
    }
    let minutos = fecha.getMinutes()
    if (minutos < 10) {
        minutos = '0' + minutos
    }
    let segundos = fecha.getSeconds()
    if (segundos < 10) {
        segundos = '0' + segundos
    }
    const body = document.querySelector('body')
    body.innerHTML = `
    <main id="container">
    <div class="itemDate" id="itemHour">${hora}</div>
    <div class="itemDate itemDoblePoint" id="doblePoint1">:</div>
    <div class="itemDate" id="itemMinuts">${minutos}</div>
    <div class="itemDate itemDoblePoint" id="doblePoint2">:</div>
    <div class="itemDate" id="itemSeconds">${segundos}</div>
    </main>
    <footer>2021 Author: davidFdzMorilla</footer>
    `
    const interval = setInterval(actualizarHora, 1000)
    interval
}

actualizarHora()